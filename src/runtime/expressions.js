var ls;
(function (ls) {
	ls.MainScene = function() {
		return {
			"200%2BMath.random()*220": function() { return 200+Math.random()*220 },
			"enemy1": function() { return enemy1 },
			"%22exp%22": function() { return "exp" },
			"%22lessOrEqual%22": function() { return "lessOrEqual" },
			"Math.pow(3%2Cplayer.lv)": function() { return Math.pow(3,player.lv) },
			"%22lessThan%22": function() { return "lessThan" },
			"player.attackrange": function() { return player.attackrange },
			"player.y": function() { return player.y },
			"%22lv%22": function() { return "lv" },
			"%22%E7%AD%89%E7%BA%A7%EF%BC%9A%22%2Bplayer.lv": function() { return "等级："+player.lv },
			"%22attack%22": function() { return "attack" },
			"1200%2BMath.random()*80": function() { return 1200+Math.random()*80 },
			"%22greaterThan%22": function() { return "greaterThan" },
			"%22%E5%89%A9%E4%BD%99%E6%95%8C%E4%BA%BA%E6%95%B0%EF%BC%9A%22%2BSystem.count": function() { return "剩余敌人数："+System.count },
			"%22%E6%94%BB%E5%87%BB%E5%8A%9B%EF%BC%9A%22%2Bplayer.attack": function() { return "攻击力："+player.attack },
			"%22attackrange%22": function() { return "attackrange" },
			"player.x": function() { return player.x },
			"%22hp%22": function() { return "hp" },
			"%22money%22": function() { return "money" },
			"gold.money": function() { return gold.money },
			"%22EXP%3A%22%2Bplayer.exp": function() { return "EXP:"+player.exp },
			"player.attack": function() { return player.attack },
			"zidan": function() { return zidan },
			"%22%E6%94%BB%E5%87%BB%E8%B7%9D%E7%A6%BB%EF%BC%9A%22%2Bplayer.attackrange": function() { return "攻击距离："+player.attackrange },
			"%22count%22": function() { return "count" },
			"%22greaterOrEqual%22": function() { return "greaterOrEqual" },
			"%22GameOver%22": function() { return "GameOver" }
		}
	};
})(ls || (ls = {}));